package demo.exception.ex2;

public class TryFinally {
    public static void main(String[] args) {
        try {
            methodA();
        } catch (Exception e) {
            System.out.println("Ловим из A");
        }
        methodB();
    }

    private static void methodA() {
        try {
            System.out.println("Вошли в метод A");
            throw new RuntimeException("demo");
        } finally {
            System.out.println("finally метода A");
        }
    }

    private static void methodB() {
        try {
            System.out.println("Вошли в метод B");
            return;
        } finally {
            System.out.println("finally метода B");
        }
    }
}

