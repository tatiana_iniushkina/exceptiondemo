package demo.exception.percent;

import java.util.Scanner;

public class PercentDemo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите процент скидки, %: ");
        int percent = scanner.nextInt();
        double fraction = 0;
        try {
            fraction = convertPercentToFraction(40);
            fraction = convertPercentToFraction(percent);
        } catch (PercentException e) {
            System.out.println("Процент скидки должен быть в диапазоне от 0 до 90");
            e.printStackTrace();
        }
        System.out.println(fraction);
    }

    private static double convertPercentToFraction(int percent) throws PercentException {
        if (percent < 0 || percent > 90) {
            throw new PercentException("Процент " + percent + " не входит в диапазон от 0 до 90");
        }
        return percent / 100.;
    }

}
