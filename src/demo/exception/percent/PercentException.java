package demo.exception.percent;

@SuppressWarnings("unused")
public class PercentException extends Exception {
    private String message;

    public PercentException() {
        message = null;
    }

    PercentException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "PercentException{" +
                "message='" + message + '\'' +
                '}';
    }
}
