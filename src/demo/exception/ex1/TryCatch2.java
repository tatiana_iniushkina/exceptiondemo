package demo.exception.ex1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TryCatch2 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String... args) {
        try {
            int x = getInteger();
            int y = 42 / x;
            System.out.println("y = " + y);
        } catch (Exception e) {
            System.out.println("Что-то пошло не так(");
//            e.printStackTrace();
        }
    }

    private static int getInteger() {
        System.out.print("Введите целое число:");
        return scanner.nextInt();
    }
}
