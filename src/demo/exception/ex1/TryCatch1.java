package demo.exception.ex1;

import java.util.Scanner;

public class TryCatch1 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String... args) {
        System.out.print("Введите целое число: ");
        int x = scanner.nextInt();
        int y = 42 / x;
        System.out.println("y = " + y);
    }
}
