package demo.exception.ex1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TryCatch3 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String... args) {
        try {
            int x = getInteger();
            int y = 42 / x;
            System.out.println("y = " + y);
        } catch (ArithmeticException e) {
            System.out.println("Деление на ноль!");
//            e.printStackTrace();
        } catch (InputMismatchException e) {
            System.out.println("Число все-таки должно быть целым");
//            e.printStackTrace();
        }
    }

    private static int getInteger() {
        System.out.print("Введите целое число:");
        return scanner.nextInt();
    }
}
